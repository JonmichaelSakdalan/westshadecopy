import React from 'react';
import { Container, Image, Col, Row } from 'react-bootstrap';
import './css/product.css'

const Versatile = () =>{

	return(
		<Container className="text-center">
			<h3 className="pt-5 pb-5">Versatile Tent</h3>
			<Row className="pt-3">
				<Col  md={4} sm={18}>
					<Image src="https://westshade.com/images/product/canopy-tent/Versatile_tent_1.jpg?auto=format&fit=max&w=1920" id="verImg" fluid/>
				</Col>
				<Col  md={4} sm={18}>
					<Image src="https://westshade.com/images/product/canopy-tent/Versatile_tent_2.jpg?auto=format&fit=max&w=1920" id="verImg" fluid/>
				</Col>
				<Col  md={4} sm={18}>
					<Image src="https://westshade.com/images/product/canopy-tent/Versatile_tent_3.jpg?auto=format&fit=max&w=1920" id="verImg" fluid/>
				</Col>
			</Row>			
			<Row className="pt-3">
				<Col  md={6} sm={18}>
					<Image src="https://westshade.com/images/product/canopy-tent/Versatile_tent_4.jpg?auto=format&fit=max&w=1920" id="verImg2" fluid/>
				</Col>
				<Col  md={6} sm={18}>
					<Image src="https://westshade.com/images/product/canopy-tent/Versatile_tent_5.jpg?auto=format&fit=max&w=1920" id="verImg2" fluid/>
				</Col>
			</Row>
		</Container>
	)
}

export default Versatile;