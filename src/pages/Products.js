import React, { useState } from 'react';
import { Container, Row, Col, Button, Image, Carousel, Card, CardGroup } from 'react-bootstrap';
import Tent from '../components/Tent';
import './css/product.css'
import SizeGuide from '../components/SizeGuide'
import CompareFrame from '../components/CompareFrame'
import Wall from '../components/Wall'
import RightWall from '../components/RightWall'
import FrontWall from '../components/FrontWall'
import BackWall from '../components/BackWall'

const Product = () => {

	const [tentImg, setTentImg] = useState("https://westshade.com/images/product/y7-heavy-duty-canopy-tent/frame/Y7-10X10-WH.png")

	const [colorImg, setColorImg] = useState("WH")

	const [frameImg, setFrameImg] = useState("Y7")

	const [frameImgLong, setFrameImgLong] = useState("y7-heavy-duty-canopy-tent")

	const [enable, setEnable] = useState(true)

	const [opt, setOpt] = useState(true)

	const [modalShow, setModalShow] = useState(false);

	const [modalShow1, setModalShow1] = useState(false);

	const [modalShow2, setModalShow2] = useState(false);

	const white = (e) =>{
		e.preventDefault()
		setColorImg("WH")
	}	
	const black = (e) =>{
		e.preventDefault()
		setColorImg("BK")
	}	
	const red = (e) =>{
		e.preventDefault()
		setColorImg("RD")
	}	
	const yellow = (e) =>{
		e.preventDefault()
		setColorImg("YE")
	}	
	const blue = (e) =>{
		e.preventDefault()
		setColorImg("BU")
	}
	const green = (e) =>{
		e.preventDefault()
		setColorImg("GN")
	}

	const y7 = (e) =>{
		e.preventDefault()
		setFrameImg("Y7")
		setFrameImgLong("y7-heavy-duty-canopy-tent")
		setEnable(true)
	}	

	const y6 = (e) =>{
		e.preventDefault()
		setFrameImg("Y6")
		setFrameImgLong("y6-commercial-buy")
		setEnable(false)
	}	

	const y5 = (e) =>{
		e.preventDefault()
		setFrameImg("Y5")
		setFrameImgLong("y5-economic-canopy-tent")
		setEnable(false)
	}
		
	const tenX10 = (e) =>{
		e.preventDefault()
		setTentImg(`https://westshade.com/images/product/${frameImgLong}/frame/${frameImg}-10X10-${colorImg}.png`)
	}	

	const tenX15 = (e) =>{
		e.preventDefault()
		setTentImg(`https://westshade.com/images/product/${frameImgLong}/frame/${frameImg}-10X15-${colorImg}.png`)
	}

	const tenX20 = (e) =>{
		e.preventDefault()
		setTentImg(`https://westshade.com/images/product/${frameImgLong}/frame/${frameImg}-10X20-${colorImg}.png`)
	}

	const thirteenX13 = (e) =>{
		e.preventDefault()
		setTentImg(`https://westshade.com/images/product/${frameImgLong}/frame/${frameImg}-13X13-${colorImg}.png`)
	}

	const thirteenX20 = (e) =>{
		e.preventDefault()
		setTentImg(`https://westshade.com/images/product/${frameImgLong}/frame/${frameImg}-13X20-${colorImg}.png`)
	}

	const thirteenX26 = (e) =>{
		e.preventDefault()
		setTentImg(`https://westshade.com/images/product/${frameImgLong}/frame/${frameImg}-13X26-${colorImg}.png`)
	}

	const sixteenX16 = (e) =>{
		e.preventDefault()
		setTentImg(`https://westshade.com/images/product/${frameImgLong}/frame/${frameImg}-16X16-${colorImg}.png`)
	}

	const twentyx20 = (e) =>{
		e.preventDefault()
		setTentImg(`https://westshade.com/images/product/${frameImgLong}/frame/${frameImg}-20X20-${colorImg}.png`)
	}

	const basic = (e) =>{
		e.preventDefault()
		setOpt(true)
	}

	const wall = (e) =>{
		e.preventDefault()
		setOpt(false)
	}

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={8} >
					<Tent tentProp={tentImg}/>
				</Col>
				<Col lg={4} className="text-center">
					<div>
					<h3>Canopy Tent</h3>
					<p style={{color:"#63A4AD"}} className="m-4">Spec</p>
					<Row>
						<Col xs={6}>
							<Button id="btnup" onClick={basic}>Basic</Button>
						</Col>						
						<Col xs={6}>
							<Button id="btnup" onClick={wall}>+Wall</Button>
						</Col>
					</Row>
					{
						opt === true
						?
						<>
							<p id="ftBold">Size</p>
							<Row xs="auto">
								<Col md={4} xs={6} id="col0">
									<Button onClick={tenX10} id="sizeBtn">10x10</Button>
								</Col>
								<Col md={4} xs={6} id="col0">
									<Button onClick={tenX15} id="sizeBtn">10x15</Button>
								</Col>
								<Col md={4} xs={6} id="col0">
									<Button onClick={tenX20} id="sizeBtn">10x20</Button>
								</Col>
								{
									enable === true
									?
										<>
											<Col md={4} xs={6} id="col0">
												<Button type="radio" onClick={thirteenX13} id="sizeBtn">13x13</Button>
											</Col>
											<Col md={4} xs={6} id="col0">
												<Button type="radio" onClick={thirteenX20} id="sizeBtn">13x20</Button>
											</Col>
											<Col md={4} xs={6} id="col0">
												<Button type="radio" onClick={thirteenX26} id="sizeBtn">13x26</Button>
											</Col>
											<Col  md={4} xs={6} id="col0">
												<Button type="radio" onClick={sixteenX16} id="sizeBtn">16x16</Button>
											</Col>
											<Col  md={4} xs={6} id="col0">	
												<Button type="radio" onClick={twentyx20} id="sizeBtn">20x20</Button>
											</Col>
										</>
									:
									<>
									</>
								}
							</Row>
							<div className="mt-2 mb-2">
								<button type="submit"  onClick={() => setModalShow(true)} id="szBtm">Size Guide</button>
								<SizeGuide show={modalShow}  onHide={() => setModalShow(false)}/>
							</div>
							<p id="ftBold" className="mt-4">Frame</p>
							<Row>
								<Col>
									<Button type="radio" onClick={y7} id="frameBtn">Y7 Heavy Duty Aluminum</Button>
									<Button type="radio" onClick={y6} id="frameBtn">Y6 Commercial Aluminum</Button>
									<Button type="radio" onClick={y5} id="frameBtn">Y5 Economic Steel</Button>
								</Col>
							</Row>
						<div className="mt-2 mb-2">
							<button type="submit"  onClick={() => setModalShow1(true)} id="szBtm1">Compare Frames</button>
							<CompareFrame show={modalShow1}  onHide={() => setModalShow1(false)}/>
						</div>
						<p id="ftBold">Color</p>
						<Container style={{minWidth:"288px", maxWidth:"389px"}}>
							<Row>
								<Col xs={18}>
									 <Button id="chngClr" style={{backgroundColor:"#FFFFFF"}} onClick={white}></Button>
									 <Button id="chngClr" style={{backgroundColor:"#000000"}} onClick={black}></Button>
									 <Button id="chngClr" style={{backgroundColor:"#991F34"}} onClick={red}></Button>
									 <Button id="chngClr" style={{backgroundColor:"#F4C84E"}} onClick={yellow}></Button>
									 <Button id="chngClr" style={{backgroundColor:"#1A4A8B"}} onClick={blue}></Button>
									 <Button id="chngClr" style={{backgroundColor:"#275D3D"}} onClick={green}></Button>
								</Col>
							</Row>
						</Container>
						</>
						:
						<>
							<Container className="mt-3">
								<Row>
									<Col xs={6}>
										<img src="https://westshade.com/images/icon/icon-wall-left.png"/><p>None</p>
									</Col>								
									<Col xs={6}>
										<Wall show={modalShow2}  onHide={() => setModalShow2(false)} wallPropTent={tentImg}/>
									</Col>									
									<Col xs={6}>
										<img src="https://westshade.com/images/icon/icon-wall-right.png"/><p>None</p>
									</Col>								
									<Col xs={6}>
										<RightWall show={modalShow2}  onHide={() => setModalShow2(false)} wallPropTent={tentImg}/>
									</Col>									
									<Col xs={6}>
										<img src="https://westshade.com/images/icon/icon-wall-front.png"/><p>None</p>
									</Col>								
									<Col xs={6}>
										<FrontWall show={modalShow2}  onHide={() => setModalShow2(false)} wallPropTent={tentImg}/>
									</Col>									
									<Col xs={6}>
										<img src="https://westshade.com/images/icon/icon-wall-back.png"/><p>None</p>
									</Col>								
									<Col xs={6}>
										<BackWall show={modalShow2}  onHide={() => setModalShow2(false)} wallPropTent={tentImg}/>
									</Col>									
								</Row>
							</Container>
						</>
					}
					</div>
				</Col>
			</Row>
		</Container>
	)
}

export default Product;