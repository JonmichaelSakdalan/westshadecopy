import React from 'react';
import { Container, Image } from 'react-bootstrap';


const Anatomy = () =>{
	return(
		<Container className="text-center mt-5" style={{backgroundColor:"#F7F7F7"}}>
			<h3 className="pt-5">The anatomy of frame</h3>
			<Image src="https://westshade.com/images/product/canopy-tent/anatomy-y7.png?auto=format&fit=max&w=1920"fluid/>
		</Container>
	)
}

export default Anatomy;
