import React from 'react';
import { Container, Image, Card, CardGroup } from 'react-bootstrap';
import './css/product.css'

const Features = () => {
	return(
		<Container>
		<div className="text-center">
			<h3 className="pt-5">Features</h3>
			<CardGroup>
			  <Card>
			    <Card.Img style={{height:"172px"}} fluid variant="top" src="https://westshade.com/images/product/canopy-tent/feature-fabric.jpg?auto=format&fit=max&w=1920" />
			    <Card.Body style={{backgroundColor:"#F5F6F8"}}>
			      <Card.Title>Roof Top</Card.Title>
			      <Card.Text>
			        Our waterproof pop tents are designed to offer the ideal coverage and protection needed for all your events. It is easy to clean, maintain and is also mold resistant for longer durability, making it ideal for all weather conditions.
			      </Card.Text>
			    </Card.Body>
			  </Card>				  
			  <Card>
			    <Card.Img style={{height:"172px"}}  variant="top" src="https://westshade.com/images/product/canopy-tent/feature-fire.png?auto=format&fit=max&w=1920" />
			    <Card.Body style={{backgroundColor:"#F5F6F8"}}>
			      <Card.Title>Roof Top</Card.Title>
			      <Card.Text>
			        Our canopies are certified under the California State Fire Marshal. Each fire retardant canopy is specially treated and complies with all NFPA 701 and CPAI-84.
			      </Card.Text>
			    </Card.Body>
			  </Card>
			  <Card>
			    <Card.Img style={{height:"172px"}}  variant="top" src="https://westshade.com/images/product/canopy-tent/feature-uv.jpg?auto=format&fit=max&w=1920" />
			    <Card.Body style={{backgroundColor:"#F5F6F8"}}>
			      <Card.Title>Roof Top</Card.Title>
			      <Card.Text>
			        Westshade canopies provide up to 98% UV block, the optimal UV protection for people and pets. Our unique polyester fabric allows warm air to escape, keeping you cool on hot and sunny days.
			      </Card.Text>
			    </Card.Body>
			  </Card>
			</CardGroup>
		</div>			
		<div className="text-center">
			<CardGroup>
			  <Card>
			    <Card.Img variant="top" src="https://westshade.com/images/product/canopy-tent/feature-steel.png?auto=format&fit=max&w=1920" id="featImg" fluid/>
			    <Card.Body style={{backgroundColor:"#F5F6F8"}}>
			      <Card.Title>Frame</Card.Title>
			      <Card.Text>
			       We carry steel frames for our Y5 canopies. Steel framed canopies are heavier and typically used for patio, garden, or the deck.
			      </Card.Text>
			    </Card.Body>
			  </Card>				  
			  <Card>
			    <Card.Img variant="top" src="https://westshade.com/images/product/canopy-tent/feature-aluminum.png?auto=format&fit=max&w=1920" id="featImg" fluid/>
			    <Card.Body style={{backgroundColor:"#F5F6F8"}}>
			      <Card.Title>Frame</Card.Title>
			      <Card.Text>
			      Our Aluminum frames (Y6, Y7) are lightweight and are used for a variety of occasions such as business events, job fairs, and exhibitions.
			      </Card.Text>
			    </Card.Body>
			  </Card>
			</CardGroup>
		</div>
		</Container>
	)
}

export default Features;