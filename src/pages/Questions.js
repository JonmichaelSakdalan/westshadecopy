import React, { useState } from 'react';
import { Container, Image, Col, Row } from 'react-bootstrap';
import './css/product.css'

const Questions = () => {

	const [enable, setEnable] = useState(false);
	const [enable2, setEnable2] = useState(false);
	const [enable3, setEnable3] = useState(false);
	const [enable4, setEnable4] = useState(false);
	const [enable5, setEnable5] = useState(false);

	const show =()=>{
		setEnable(!enable)
	}
	const show2 =()=>{
		setEnable2(!enable2)
	}
	const show3 =()=>{
		setEnable3(!enable3)
	}
	const show4 =()=>{
		setEnable4(!enable4)
	}
	const show5 =()=>{
		setEnable5(!enable5)
	}

	return(
		<Container className="text-jutify">
			<h3 className="pt-5 pb-5 text-center">Let’s answer your questions</h3>
			<p onClick={show} id="qFont">Do your canopies set up in seconds?</p>
				{
					enable === true
					?
					<p>They sure do! Our canopies can be set up in less than 60 seconds with just two people.</p>
					:
					<>
					</>
				}
			<p onClick={show2} id="qFont">Do you have a video showing proper setup and take down??</p>
				{
					enable2 === true
					?
					<p>Yes! Check out this one minute video https://www.youtube.com/watch?v=J9ygFXvOVn4</p>
					:
					<>
					</>
				}
			<p onClick={show3} id="qFont">Can my canopy withstand wind and at what point are weight bags or steel stakes required?</p>
				{
					enable3 === true
					?
					<p>We recommend using weight bags or steel stakes in all types of weather environments. White stakes are ideal to keep your canopy secure during all outdoor activities, our professional weight bags hold up to 30lbs of sand, or anything similar material, and easily attach to your shelter for additional stability.</p>
					:
					<>
					</>
				}
			<p onClick={show4} id="qFont">Can I use my canopy anywhere?</p>
				{
					enable4 === true
					?
					<p>Yes, our canopies stand securely on grass, dirt, or pavement without ropes and poles. In windy conditions, however, we recommend using our weight bags to anchor and prevent your canopy from tipping over</p>
					:
					<>
					</>
				}
			<p onClick={show5} id="qFont">I bought a canopy from another company, will your replacement fit my current frame?</p>
				{
					enable5 === true
					?
					<p>Our tops are designed to fit Westshade brand frames. We do not recommend using our frame or top with another company's product.</p>
					:
					<>
					</>
				}
			<div id="filler">
			</div>
		</Container>
	)
}

export default Questions;