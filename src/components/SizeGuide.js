import React from 'react';
import { Modal, Button, Table, Image } from 'react-bootstrap';
import './css/sizeguide.css';

const SizeGuide = (sizeProp) =>{
	return(
		<Modal
		  {...sizeProp}
		  size="lg"
		  aria-labelledby="contained-modal-title-vcenter"
		  centered
		>
		  <Modal.Header closeButton>
		  </Modal.Header>
		  <Modal.Body>
		    <h4>Size Guide</h4>
		    <Table borderless>
		    	<thead>
		    		<tr>
		    			<th></th>
		    			<th><Image src="https://westshade.com/images/icon/icon-10x10.png?auto=format&fit=max&w=1920&q=100" id="imgmSize"/></th>
		    			<th><Image src="https://westshade.com/images/icon/icon-10x15.png?auto=format&fit=max&w=1920&q=100" id="imgmSize"/></th>
		    			<th><Image src="https://westshade.com/images/icon/icon-13x13.png?auto=format&fit=max&w=1920&q=100" id="imgmSize"/></th>
		    			<th><Image src="https://westshade.com/images/icon/icon-10x20.png?auto=format&fit=max&w=1920&q=100" id="imgmSize"/></th>
		    			<th><Image src="https://westshade.com/images/icon/icon-16x16.png?auto=format&fit=max&w=1920&q=100" id="imgmSize"/></th>
		    			<th><Image src="https://westshade.com/images/icon/icon-13x20.png?auto=format&fit=max&w=1920&q=100" id="imgmSize"/></th>
		    			<th><Image src="https://westshade.com/images/icon/icon-13x26.png?auto=format&fit=max&w=1920&q=100" id="imgmSize"/></th>
		    			<th><Image src="https://westshade.com/images/icon/icon-20x20.png?auto=format&fit=max&w=1920&q=100" id="imgmSize"/></th>
		    		</tr>
		    	</thead>
		    	<tbody className="text-center">
		    		<tr>
		    			<td><strong>Y5 Economic steel</strong><p style={{color:"#8D8D8D",fontSize:"14.36px",margin:"0px"}}>Clearance Height 6'10"</p><p style={{color:"#8D8D8D",fontSize:"14.36px",margin:"0px"}}>Overall Height 10'15"</p></td>
		    			<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/unrelated.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/unrelated.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/unrelated.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/unrelated.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/unrelated.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    		</tr>		    		
		    		<tr>
		    			<td><strong>Y5 Economic steel</strong><p style={{color:"#8D8D8D",fontSize:"14.36px",margin:"0px"}}>Clearance Height 6'10"</p><p style={{color:"#8D8D8D",fontSize:"14.36px",margin:"0px"}}>Overall Height 10'15"</p></td>
			    		<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
			    		<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
			    		<td><Image src="https://westshade.com/images/umbrella/unrelated.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
			    		<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
			    		<td><Image src="https://westshade.com/images/umbrella/unrelated.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
			    		<td><Image src="https://westshade.com/images/umbrella/unrelated.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
			    		<td><Image src="https://westshade.com/images/umbrella/unrelated.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
			    		<td><Image src="https://westshade.com/images/umbrella/unrelated.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    		</tr>		    		
		    		<tr>
		    			<td><strong>Y5 Economic steel</strong><p style={{color:"#8D8D8D",fontSize:"14.36px",margin:"0px"}}>Clearance Height 6'10"</p><p style={{color:"#8D8D8D",fontSize:"14.36px",margin:"0px"}}>Overall Height 10'15"</p></td>
		    			<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    			<td><Image src="https://westshade.com/images/umbrella/related.png?auto=format&fit=max&w=1280&q=100" id="check"/></td>
		    		</tr>
		    	</tbody>
		    </Table>
		  </Modal.Body>
		</Modal>	
	)
}

export default SizeGuide;