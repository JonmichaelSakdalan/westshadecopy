import React, { useState } from 'react';
import { Modal, Button, Image, Container, Row, Col } from 'react-bootstrap';
import './css/wall.css';
import Tent from './Tent';

const FrontWall = ({wallPropImg, wallPropTent}) => {
	const [show, setShow] = useState(false);
	const [wallImg, setWallImg] = useState(``);
	const [color, setColor] = useState(`WH`);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const full = (e) => {
		e.preventDefault()


		setWallImg(`https://westshade.com/images/product/y7-heavy-duty-canopy-tent/wall/Y7-FW10${color}-D.png`)
	}	
	const half = (e) => {
		e.preventDefault()


		setWallImg(`https://westshade.com/images/product/y7-heavy-duty-canopy-tent/wall/Y7-HW10${color}-D.png`)
	}	
	const mesh = (e) => {
		e.preventDefault()


		setWallImg(`https://westshade.com/images/product/y7-heavy-duty-canopy-tent/wall/Y7-MW10${color}-D.png`)
	}	
	const pvc = (e) => {
		e.preventDefault()


		setWallImg(`https://westshade.com/images/product/y7-heavy-duty-canopy-tent/wall/Y7-PW10${color}-D.png`)
	}	
	const roll = (e) => {
		e.preventDefault()


		setWallImg(`https://westshade.com/images/product/y7-heavy-duty-canopy-tent/wall/Y7-RW10${color}-D.png`)
	}

	const white = (e) =>{
		setColor("WH")
	}
	const black = (e) =>{	
		setColor("BK")
	}	
	const red = (e) =>{
		setColor("RD")
	}	
	const yellow = (e) =>{
		setColor("YE")
	}	
	const blue = (e) =>{
		setColor("BU")
	}
	const green = (e) =>{
		setColor("GN")
	}
	const saveWall = (e) =>{
		return(
			<Tent wall={wallImg}/>

		)
	}
	const save = (e) =>{
		handleClose();
		saveWall();
	}
	
	return(
		<>
		  <Button id="wallBtn" onClick={handleShow}>
		  	Edit
		  </Button>

		  <Modal size="xl" show={show} onHide={handleClose}>
		    <Modal.Header closeButton></Modal.Header>
		    <Modal.Body>
		    	<Row className="text-center">
		    		<Col md = {6}>
		    			<Image src={wallPropTent} id="wallImg" fluid/>
		    			<Image src={wallImg} id="wallImg2" fluid/>
		    		</Col>
		    		<Col md={6}>
		    			<div>
		    				<Row>
		    					<Col md={4} xs={6}>
		    						<Button id="wallSizeBtn" onClick={full}><Image src="https://westshade.com/images/icon/wall-full.png?auto=format&fit=max&w=1920&q=100"/><p>Full</p></Button>
		    					</Col>		    					
		    					<Col md={4} xs={6}>
		    						<Button id="wallSizeBtn" onClick={half}><Image src="https://westshade.com/images/icon/wall-half.png?auto=format&fit=max&w=1920&q=100"/><p>Half</p></Button>
		    					</Col>		    					
		    					<Col md={4} xs={6}>
		    						<Button id="wallSizeBtn" onClick={mesh}><Image src="https://westshade.com/images/icon/wall-mesh.png?auto=format&fit=max&w=1920&q=100"/><p>Mesh</p></Button>
		    					</Col>		    					
		    					<Col md={4} xs={6}>
		    						<Button id="wallSizeBtn" onClick={pvc}><Image src="https://westshade.com/images/icon/wall-pvc.png?auto=format&fit=max&w=1920&q=100"/><p>PVC</p></Button>
		    					</Col>		    					
		    					<Col md={4} xs={6}>
		    						<Button id="wallSizeBtn" onClick={roll}><Image src="https://westshade.com/images/icon/wall-rollup.png?auto=format&fit=max&w=1920&q=100"/><p>Roll-up</p></Button>
		    					</Col>
		    				</Row>
		    				<Container style={{minWidth:"288px", maxWidth:"389px"}}>
		    					<Row>
		    						<Col xs={18}>
		    							 <Button id="chngClr" style={{backgroundColor:"#FFFFFF"}} onClick={white}></Button>
		    							 <Button id="chngClr" style={{backgroundColor:"#000000"}} onClick={black}></Button>
		    							 <Button id="chngClr" style={{backgroundColor:"#991F34"}} onClick={red}></Button>
		    							 <Button id="chngClr" style={{backgroundColor:"#F4C84E"}} onClick={yellow}></Button>
		    							 <Button id="chngClr" style={{backgroundColor:"#1A4A8B"}} onClick={blue}></Button>
		    							 <Button id="chngClr" style={{backgroundColor:"#275D3D"}} onClick={green}></Button>
		    						</Col>
		    					</Row>
		    				</Container>
		    			</div>
		    		</Col>
		    	</Row>
		    </Modal.Body>
		    <Modal.Footer>
		      <Button variant="secondary" onClick={handleClose}>
		        Cancel
		      </Button>
		      <Button variant="primary" onClick={save}>
		        Save
		      </Button>
		    </Modal.Footer>
		  </Modal>
		</>
	)
}

export default FrontWall;