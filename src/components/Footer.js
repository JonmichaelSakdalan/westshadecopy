import React from 'react';
import { Container, Image, Col, Row, Table } from 'react-bootstrap';
import './css/footer.css';
import { BsFacebook, BsTwitter, BsInstagram, BsYoutube, BsPinterest } from "react-icons/bs";

const Footer =()=>{
	return(
		<Container className="mt-5">
			<Row>
				<Col md={4} xs={18}>
					<a href="#" id="link"><Image src="https://westshade.com/images/icon/logo-site-dark-footer.png?auto=format&fit=max&w=1920" id="logoFoot"/></a>
				</Col>					
				<Col md={4} xs={18}>
					<Table borderless>
						<thead>
							<tr>
								<td>
									<strong>All products</strong>
								</td>
								<td>
									<strong>HELP & MORE</strong>
								</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<a href="#" id="link">Custom Printing</a>
								</td>
								<td>
									<a href="#" id="link">Contact us</a>
								</td>
							</tr>						
							<tr>
								<td>
									<a href="#" id="link">Canopy tents</a>
								</td>
								<td>
									<a href="#" id="link">Shipping & return</a>
								</td>
							</tr>						
							<tr>
								<td>
									<a href="#" id="link">Umbrellas</a>
								</td>
								<td>
									<a href="#" id="link">Warranty</a>
								</td>
							</tr>						
							<tr>
								<td>
									<a href="#" id="link">Accessoriesg</a>
								</td>
								<td>
									<a href="#" id="link">About us</a>
								</td>
							</tr>
						</tbody>
					</Table>
				</Col>				
				<Col md={4} xs={18}>
					<Table borderless>
						<thead>
							<tr>
								<td>
									<a href="#" id="link"><BsFacebook size={22}/></a>
								</td>
								<td>
									<a href="#" id="link"><BsTwitter size={22}/></a>
								</td>
								<td>
									<a href="#" id="link"><BsInstagram size={22}/></a>
								</td>
								<td>
									<a href="#" id="link"><BsYoutube size={22}/></a>
								</td>
								<td>
									<a href="#" id="link"><BsPinterest size={22}/></a>
								</td>
							</tr>
						</thead>
					</Table>
				</Col>
			</Row>
		</Container>
	)
}

export default Footer;