import React, { useState }  from 'react';
import { Navbar, Nav, Container, NavDropdown, Table, Image, Row, Col } from 'react-bootstrap';
import './css/navbar.css';
import { MdOutlineShoppingCart } from "react-icons/md";
import { CgProfile } from "react-icons/cg";


const NavBar = () => {

	const [show, setShow] = useState(false);
	const [show1, setShow1] = useState(false);

	const showDrop = (e)=>{
		setShow(!show);
	}

	const hideDrop = (e)=>{
		setShow(false);
	}	

	const showDrop1 = (e)=>{
		setShow1(!show);
	}

	const hideDrop1 = (e)=>{
		setShow1(false);
	}

	return(
		<Navbar collapseOnSelect expand="lg" bg="light" variant="light" sticky="top">
			<Container id="fontBold">
				<Navbar.Brand href="#home"><img src="https://westshade.com/images/icon/logo-site-dark-header.png?auto=format&fit=max&w=1920&q=100"/></Navbar.Brand>
			  	<Navbar.Toggle aria-controls="responsive-navbar-nav" />
			  	<Navbar.Collapse id="responsive-navbar-nav">
			    <Nav className="m-auto">
			      	<Nav.Link href="#" style={{color:"black"}}>CANOPY TENT</Nav.Link>
			      	<NavDropdown style={{color:"black"}} title="UMBRELLA" show={show} onMouseEnter={showDrop} onMouseLeave={hideDrop}>
			      		<Container>
			      			<Row>
			      				<Col md={12}>
					      			<Table borderless>
					      				<thead className="text-center">
					      					<tr>
									      		<th>
									      			<NavDropdown.Item href="#"><strong>MARKET</strong></NavDropdown.Item>
									      		</th>
									      		<th>
									      			<NavDropdown.Item href="#"><strong>TILT</strong></NavDropdown.Item>
									      		</th>
									      		<th>
									      			<NavDropdown.Item href="#"><strong>OVERSIZE</strong></NavDropdown.Item>
								      			</th>							      		
								   			</tr>
								   		</thead>
								   		<tbody className="text-center">
								   			<tr>
								   				<td>
								   					<NavDropdown.Item href="#">Marco</NavDropdown.Item>
								   				</td>						   				
								   				<td>
								   					<NavDropdown.Item href="#">Bali</NavDropdown.Item>
								   				</td>						   				
								   				<td>
								   					<NavDropdown.Item href="#">Katalina</NavDropdown.Item>
								   				</td>
								   			</tr>						   			
								   			<tr>
								   				<td>
								   					<NavDropdown.Item href="#">Santorini</NavDropdown.Item>
								   				</td>						   				
								   				<td>
								   					<NavDropdown.Item href="#">Kapri</NavDropdown.Item>
								   				</td>						   				
								   				<td>
								   					<NavDropdown.Item href="#">Learn More ></NavDropdown.Item>
								   				</td>
								   			</tr>
								   		</tbody>
								     </Table>
						     	</Col>
						     	<Col  md={12}>
									<NavDropdown.Item><Image src="https://westshade.com/images/component/header/umbrella.jpg?auto=format&fit=max&w=1920&q=100"/></NavDropdown.Item>
								</Col>
							</Row>
						</Container>
			      	</NavDropdown>
			      	<NavDropdown href="#" style={{color:"black"}} title="CUSTOM PRINTING" show={show1} onMouseEnter={showDrop1} onMouseLeave={hideDrop1}>
			      		      		<Container>
			      		      			<Row>
			      		      				<Col md={12}>
			      				      			<Table borderless>
			      							   		<tbody className="text-center">
			      							   			<tr>
			      							   				<td>
			      							   					<NavDropdown.Item href="#">Canopy Tent</NavDropdown.Item>
			      							   				</td>
			      							   			</tr>
			      							   			<tr>						   				
			      							   				<td>
			      							   					<NavDropdown.Item href="#">Umbrella</NavDropdown.Item>
			      							   				</td>
			      							   			</tr>	
			      							   			<tr>					   				
			      							   				<td>
			      							   					<NavDropdown.Item href="#">Table Cover</NavDropdown.Item>
			      							   				</td>
			      							   			</tr>
			      							   			<tr>			      							   				
			      							   				<td>
			      							   					<NavDropdown.Item href="#"><strong>Learn More ></strong></NavDropdown.Item>
			      							   				</td>
			      							   			</tr>						   			
			      							   		</tbody>
			      							     </Table>
			      					     	</Col>
			      					     	<Col  md={12}>
			      								<NavDropdown.Item><Image src="https://westshade.com/images/component/header/custom_printing.jpg?auto=format&fit=max&w=1920&q=100" style={{width:"440px"}}/></NavDropdown.Item>
			      							</Col>
			      						</Row>
			      					</Container>
			      	</NavDropdown>
			      	<Nav.Link href="#" style={{color:"black"}}>ACCESSORIES</Nav.Link>
			    </Nav>
			    <Nav>
			     	<Nav.Link href="#"><MdOutlineShoppingCart style={{color:"black"}} size={25}/></Nav.Link>
			    </Nav>
			 	 </Navbar.Collapse>
			 	<Nav>
			 	<Nav.Link href="#"><CgProfile style={{color:"black"}} size={25}/></Nav.Link>
			 	</Nav>
			</Container>
		</Navbar>

	)
}

export default NavBar;