import React from 'react';
import { Container, Card } from 'react-bootstrap';
import './css/tent.css';

const Tent = ({tentProp, wall}) => {
	return(
		<Container>
			<Card id='cardBorder'>
				<Card.Img src={tentProp} id="imgSize"/>
				{
					wall === null
					?
						<>
							<Card.Img src={wall} id="imgSize"/>
						</>
					:
						<>
						</>
				}
			</Card>
		</Container>
	)
}

export default Tent;