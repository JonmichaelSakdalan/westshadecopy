import React from 'react';
import { Modal, Button, Table, Image } from 'react-bootstrap';
import './css/sizeguide.css';

const CompareFrame = (frameProp) =>{
	return(
		<Modal
		  {...frameProp}
		  size="lg"
		  aria-labelledby="contained-modal-title-vcenter"
		  centered
		>
		  <Modal.Header closeButton>
		  </Modal.Header>
		  <Modal.Body>
		    <Table borderless>
			    <thead className="text-center">
			    	<tr>
			      		<th><p style={{color:"#8D8D8D",margin:"0px"}}>Y5 Economic steel</p><a href="#" style={{textDecoration:"none",color:"#54B8BF"}}>Learn More</a></th>
			      		<th><p style={{color:"#8D8D8D",margin:"0px"}}>Y6 Commercial Aluminum</p><a href="#" style={{textDecoration:"none",color:"#54B8BF"}}>Learn More</a></th>
			      		<th><p style={{color:"#8D8D8D",margin:"0px"}}>Y7 Heavy duty aluminum</p><a href="#" style={{textDecoration:"none",color:"#54B8BF"}}>Learn More</a></th>
			      	</tr>
			    </thead>
		    	<tbody className="text-center">
		    		<tr>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>Steel</h3><p style={{color:"#969696",margin:"0px"}}>Frame</p></td>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>Aluminum</h3><p style={{color:"#969696",margin:"0px"}}>Frame</p></td>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>Aluminum</h3><p style={{color:"#969696",margin:"0px"}}>Frame</p></td>
		    		</tr>		    		
		    		<tr>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>0.05"</h3><p style={{color:"#969696",margin:"0px"}}>Pole thickness</p></td>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>0.06"</h3><p style={{color:"#969696",margin:"0px"}}>Pole thickness</p></td>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>0.07"</h3><p style={{color:"#969696",margin:"0px"}}>Pole thickness</p></td>
		    		</tr>		    		
		    		<tr>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>45mm</h3><p style={{color:"#969696",margin:"0px"}}>Pole diameter</p></td>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>45mm</h3><p style={{color:"#969696",margin:"0px"}}>Pole diameter</p></td>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>57mm</h3><p style={{color:"#969696",margin:"0px"}}>Pole diameter</p></td>
		    		</tr>		    		
		    		<tr>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>3</h3><p style={{color:"#969696",margin:"0px"}}>Size available</p></td>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>3</h3><p style={{color:"#969696",margin:"0px"}}>Size available</p></td>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>9</h3><p style={{color:"#969696",margin:"0px"}}>PSize available</p></td>
		    		</tr>		    		
		    		<tr>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>1 year</h3><p style={{color:"#969696",margin:"0px"}}>Frame warranty</p></td>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>5 years</h3><p style={{color:"#969696",margin:"0px"}}>Frame warranty</p></td>
		    			<td><h3 style={{color:"#5D5D5D",margin:"0px"}}>10 years</h3><p style={{color:"#969696",margin:"0px"}}>Frame warranty</p></td>
		    		</tr>
		    	</tbody>
		    </Table>
		  </Modal.Body>
		</Modal>	
	)
}

export default CompareFrame;