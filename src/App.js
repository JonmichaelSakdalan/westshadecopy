import React from 'react';
import './App.css';
import NavBar from './components/Navbar';
import Product from './pages/Products';
import Anatomy from './pages/Anatomy';
import Features from './pages/Features';
import Versatile from './pages/Versatile';
import Questions from './pages/Questions';
import Footer from './components/Footer';

const App = () => {
  return (
    <>
    <NavBar/>
    <Product/>
    <Anatomy/>
    <Features/>
    <Versatile/>
    <Questions/>
    <Footer/>
    </>
  );
}

export default App;
